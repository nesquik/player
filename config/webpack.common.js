const { CleanWebpackPlugin } = require('clean-webpack-plugin')
const PrettierPlugin = require('prettier-webpack-plugin')
const ESLintPlugin = require('eslint-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const path = require('path')

module.exports = {
  // Where webpack looks to start building the bundle
  entry: [path.resolve(__dirname, '../src/index.js')],

  // Where webpack outputs the assets and bundles
  output: {
    path: path.resolve(__dirname, '../dist'),
    filename: 'hinted-widget.js',
  },

  // Customize the webpack build process
  plugins: [
    // Removes/cleans build folders and unused assets when rebuilding
    new CleanWebpackPlugin(),

    // ESLint configuration
    new ESLintPlugin({
      files: ['.', 'src', 'config'],
      formatter: 'table',
    }),

    // Prettier configuration
    new PrettierPlugin(),
  
    new VueLoaderPlugin(),
  ],

  // Determine how modules within the project are treated
  module: {
    rules: [
      // JavaScript: Use Babel to transpile JavaScript files
      { test: /\.js$/, use: ['babel-loader'] },

      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
  
      {
        test: /\.(scss|css)$/,
        use: [
          'vue-style-loader',
          {
            loader: 'css-loader',
          },
          'postcss-loader',
          'sass-loader',
        ],
      },
    ],
  },

  resolve: {
    modules: [path.resolve(__dirname, '../src'), 'node_modules'],
    extensions: ['.js', '.jsx', '.json', '.vue'],
    alias: {
      '@': path.resolve(__dirname, '../src'),
    },
  },
  
  stats: 'minimal',
}
