const { merge } = require('webpack-merge')

const common = require('./webpack.common.js')

module.exports = merge(common, {
  mode: 'production',
  
  devtool: process.env.INCLUDE_SOURCE_MAPS ? 'inline-source-map' : false,
})
