const { merge } = require('webpack-merge')
const HtmlWebpackPlugin = require('html-webpack-plugin');

const common = require('./webpack.common.js')

module.exports = merge(common, {
  // Set the mode to development or production
  mode: 'development',

  // Control how source maps are generated
  devtool: 'inline-source-map',

  // Spin up a server for quick development
  devServer: {
    compress: true,
    port: 8000,
    hot: true,
    open: true,
  },

  plugins: [
    // Only update what has changed on hot reload
    new HtmlWebpackPlugin({
      template: './public/index.html',
      scriptLoading: 'blocking',
      inject: 'head',
      contentBase: ['../src', '../public'],
    }),
  ],
})
