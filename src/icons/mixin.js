export default {
  props: {
    width: {
      type: [String, Number],
      required: false,
    },
    height: {
      type: [String, Number],
      required: false,
    },
  },
};
