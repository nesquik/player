import Vue from 'vue';

import App from './App.vue';
import store from './store';
import { i18n } from './libs/i18n';

let widget = null;
const idWidget = 'hinted-widget';

Vue.config.productionTip = false;

const init = () => {
  if (widget) {
    return;
  }

  const el = document.createElement('div');
  el.id = idWidget;

  if (document.body) {
    document.body.appendChild(el);

    widget = new Vue({
      store,
      i18n,
      render: (h) => h(App),
    }).$mount(`#${idWidget}`);
  }
};

const intervalTimer = setInterval(init, 2000);

setTimeout(() => {
  clearInterval(intervalTimer);
}, 10000);
