import VueI18n from 'vue-i18n';
import Vue from 'vue';
import { getUserLocale } from 'get-user-locale';
import localeCode from 'locale-code';

import en from '../locales/en';
import ru from '../locales/ru';

Vue.use(VueI18n);

const getCurrentLocale = () => {
  const userLocale = getUserLocale();

  if (userLocale === 'ru') {
    return userLocale;
  }

  return localeCode.getLanguageCode(userLocale);
};

const messages = {
  en,
  ru,
};

const i18Instance = new VueI18n({
  locale: getCurrentLocale(),
  fallbackLocale: 'en',
  messages,
});

export const i18n = i18Instance;
